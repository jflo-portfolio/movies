# movies

movies is a Python library that scrapes imdb for movie information.


## Installation

### Python version

Python (>= 3.6)

### Install movies

    pip install git+https://bitbucket.org/jflo-portfolio/movies.git

## Usage

import module

        >>> import movies

create a celeb object

        >>> celeb = movies.celeb("Mark Hamill")

information available for a celeb:

        >>> celeb.name
        >>> celeb.overview
        >>> celeb.bio
        >>> celeb.filmography

create a title object

        >>> title = movies.title("Dune")

information available for a title object:

        >>> title.name
        >>> title.year
        >>> title.rating
        >>> title.runtime
        >>> release_date
        >>> description
        >>> summary
        >>> written_by
        >>> directed_by
        >>> cast
        >>> editor
        >>> music_department
