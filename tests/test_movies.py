from movies import __version__
from movies.api import celeb
from movies.api import title


def test_version():
    assert __version__ == "0.1.0"


def test_celeb():
    """celeb test"""
    c = celeb("Mark Hamill")
    assert c.name == "Mark Hamill (I)"
    assert c.overview["Born"] == "September 25, 1951 in Oakland, California, USA"
    assert c.overview["Birth Name"] == "Mark Richard Hamill"
    assert c.overview["Height"] == "5' 7¾\" (1.72 m)"
    assert "Mark Hamill is best known for his portrayal of Luke Skywalker" in c.bio
    sample_films = [
        "Star Wars: Episode IV - A New Hope",
        "Star Wars: Episode V - The Empire Strikes Back",
        "Star Wars: Episode VI - Return of the Jedi",
        "Star Wars: Episode VII - The Force Awakens",
        "Star Wars: Episode VIII - The Last Jedi",
    ]
    for sample in sample_films:
        if sample in c.filmography:
            assert True
        else:
            assert False


def test_title():
    """title test"""
    t = title("Dune")
    assert t.title == "Dune"
    assert t.year == "2021"
    assert t.rating == "PG-13"
    assert t.runtime == "2h 35m"
    assert t.release_date == "October 22, 2021 (United States)"
    assert (
        "Feature adaptation of Frank Herbert's science fiction novel" in t.description
    )
    assert "A mythic and emotionally charged hero's journey" in t.summary
    assert t.directed_by[0][0] == "Denis Villeneuve"
    assert t.directed_by[0][1] == "(directed by)"
    assert t.written_by[0] == ("Jon Spaihts", "(screenplay by) and")
    assert t.written_by[1] == ("Denis Villeneuve", "(screenplay by) and")
    sample_cast = [
        ("Timothée Chalamet", "Paul Atreides"),
        ("Rebecca Ferguson", "Lady Jessica Atreides"),
        ("Oscar Isaac", "Duke Leto Atreides"),
        ("Jason Momoa", "Duncan Idaho"),
    ]
    for cast_member in sample_cast:
        assert cast_member in t.cast
    assert t.editor[0] == ("Joe Walker", "film editor")
    sample_music = [
        ("Peter Afterman", "music supervisor"),
        ("Aldo Arechar", "technical assistant"),
        ("Clint Bennett", "supervising music editor"),
    ]
    for music_member in sample_music:
        assert music_member in t.music_department
