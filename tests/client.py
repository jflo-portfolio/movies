#!/usr/bin/env python3
from movies.api import celeb
from movies.api import title


def test_celeb():
    """celeb test"""
    c = celeb("Mark Hamill")
    print(c.name)
    print(c.overview)
    print(c.bio)
    print(c.filmography)


def test_title():
    """title test"""
    t = title("Dune")
    print(t._url)
    print(t.title)
    print(t.year)
    print(t.rating)
    print(t.runtime)
    print(t.release_date)
    print(t.description)
    print(t.summary)
    print(t.directed_by)
    print(t.written_by)
    print(t.cast)
    print(t.editor)
    print(t.music_department)


def main():
    """test celeb and title"""
    test_celeb()
    test_title()


if __name__ == "__main__":
    main()
