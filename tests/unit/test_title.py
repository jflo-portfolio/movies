import pytest

import movies

from ..integration import test_title


class MockTitle:
    @property
    def title(self):
        return "Dune"

    @property
    def year(self):
        return "2021"

    @property
    def rating(self):
        return "PG-13"

    @property
    def runtime(self):
        return "2h 35m"

    @property
    def release_date(self):
        return "October 22, 2021 (United States)"

    @property
    def description(self):
        return "Feature adaptation of Frank Herbert's science fiction nove"

    @property
    def summary(self):
        return "A mythic and emotionally charged hero's journey"

    @property
    def written_by(self):
        return [("Denis Villeneuve", "(written by)")]

    @property
    def directed_by(self):
        return [("Denis Villeneuve", "(directed by)")]

    @property
    def cast(self):
        return [("Rebecca Ferguson", "")]

    @property
    def editor(self):
        return [("Joe Walker", "")]

    @property
    def music_department(self):
        return [("Peter Afterman", "")]


@pytest.fixture
def title_obj(monkeypatch):
    def mock_title(*args, **kwargs):
        return MockTitle()

    monkeypatch.setattr(movies, "title", mock_title)

    t = movies.title("Dune")
    return t


def test_name(title_obj):
    test_title.test_name(title_obj)


def test_year(title_obj):
    test_title.test_year(title_obj)


def test_rating(title_obj):
    test_title.test_rating(title_obj)


def test_runtime(title_obj):
    test_title.test_runtime(title_obj)


def test_release_date(title_obj):
    test_title.test_release_date(title_obj)


def test_description(title_obj):
    test_title.test_description(title_obj)


def test_summary(title_obj):
    test_title.test_summary(title_obj)


def test_directed_by(title_obj):
    test_title.test_directed_by(title_obj)


def test_written_by(title_obj):
    test_title.test_written_by(title_obj)


def test_cast(title_obj):
    test_title.test_cast(title_obj)


def test_editor(title_obj):
    test_title.test_editor(title_obj)


def test_music_department(title_obj):
    test_title.test_music_department(title_obj)
