import pytest

import movies

from ..integration import test_celeb


class MockCeleb:
    @property
    def name(self):
        return "Mark Hamill (I)"

    @property
    def overview(self):
        return {
            "Born": "September 25, 1951 in Oakland, California, USA",
            "Birth Name": "Mark Richard Hamill",
            "Height": "5' 7\" (1.7 m)",
        }

    @property
    def bio(self):
        return "Mark Hamill is best known for his portrayal of Luke Skywalker"

    @property
    def filmography(self):
        return ["Squadron 42", "The Fall of the House of Usher", "Relatively Super"]


@pytest.fixture
def celeb_obj(monkeypatch):
    def mock_celeb(*args, **kwargs):
        return MockCeleb()

    monkeypatch.setattr(movies, "celeb", mock_celeb)

    c = movies.celeb("Mark Hammill")
    return c


def test_name(celeb_obj):
    test_celeb.test_name(celeb_obj)


def test_overview(celeb_obj):
    test_celeb.test_overview(celeb_obj)


def test_bio(celeb_obj):
    test_celeb.test_bio(celeb_obj)


def test_filmography(celeb_obj):
    test_celeb.test_filmography(celeb_obj)
