import pytest

from movies import title


@pytest.fixture(scope="module")
def title_obj():
    t = title("Dune")
    return t


def test_name(title_obj):
    print(title_obj.title)
    assert title_obj.title == "Dune"


def test_year(title_obj):
    print(title_obj.year)
    assert title_obj.year == "2021"


def test_rating(title_obj):
    print(title_obj.rating)
    assert title_obj.rating == "PG-13"


def test_runtime(title_obj):
    print(title_obj.runtime)
    assert title_obj.runtime == "2h 35m"


def test_release_date(title_obj):
    print(title_obj.release_date)
    assert title_obj.release_date == "October 22, 2021 (United States)"


def test_description(title_obj):
    print(title_obj.description)
    assert title_obj.description.startswith(
        "Feature adaptation of Frank Herbert's science fiction nove"
    )


def test_summary(title_obj):
    print(title_obj.summary)
    assert title_obj.summary.startswith(
        "A mythic and emotionally charged hero's journey"
    )


def test_directed_by(title_obj):
    print(title_obj.directed_by)
    assert title_obj.directed_by == [("Denis Villeneuve", "(directed by)")]


def test_written_by(title_obj):
    print(title_obj.written_by)
    assert "Denis Villeneuve" in [w[0] for w in title_obj.written_by]


def test_cast(title_obj):
    print(title_obj.cast)
    assert "Rebecca Ferguson" in [c[0] for c in title_obj.cast]


def test_editor(title_obj):
    print(title_obj.editor)
    assert "Joe Walker" in [e[0] for e in title_obj.editor]


def test_music_department(title_obj):
    print(title_obj.music_department)
    assert "Peter Afterman" in [m[0] for m in title_obj.music_department]
