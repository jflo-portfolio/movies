import pytest

from movies import celeb


@pytest.fixture(scope="module")
def celeb_obj():
    c = celeb("Mark Hamill")
    return c


def test_name(celeb_obj):
    print(celeb_obj.name)
    assert celeb_obj.name == "Mark Hamill (I)"


def test_overview(celeb_obj):
    print(celeb_obj.overview)
    assert (
        celeb_obj.overview["Born"] == "September 25, 1951 in Oakland, California, USA"
    )
    assert celeb_obj.overview["Birth Name"] == "Mark Richard Hamill"
    assert celeb_obj.overview["Height"] == "5' 7\" (1.7 m)"


def test_bio(celeb_obj):
    print(celeb_obj.bio)
    assert celeb_obj.bio.startswith(
        "Mark Hamill is best known for his portrayal of Luke Skywalker"
    )


def test_filmography(celeb_obj):
    print(celeb_obj.filmography)
    film_subset = ["Squadron 42", "The Fall of the House of Usher", "Relatively Super"]
    for film in film_subset:
        assert film in celeb_obj.filmography
