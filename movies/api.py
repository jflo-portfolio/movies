from movies.data.celeb import Celeb
from movies.data.title import Title


def celeb(celeb_name):
    """Return a Celeb object

    :param celeb_name: the name of a movie celeb
    :type celeb_name: str
    :return: a Celeb object
    :rtype: class:`movies.data.celeb.Celeb`
    """
    return Celeb(celeb_name)


def title(title_name):
    """Return a Title object

    :param title_name: the name of a movie/tv show
    :type title_name: str
    :return: a Title object
    :rtype: class:`movies.data.title.Title`
    """
    return Title(title_name)
