from movies.data.movie_data import MovieData
from movies.utils import remove_spaces


class Title(MovieData):
    """This is a class representing a movie title

    :param title: title of a movie
    :type title: str
    """

    def __init__(self, title):
        """Constructor method"""
        super().__init__()
        self._url = super().search(title, qtype="title")
        self._page = self._parse_page(self._url)
        self._fullcredits_url = self._url + "fullcredits"
        self._fullcredits_page = self._parse_page(self._fullcredits_url)
        self._meta_data = self._get_meta_data()
        self._avaiable_crew_types = self._get_available_crew_types()

        for crew_type in self._avaiable_crew_types:
            if crew_type != "cast":
                setattr(self, crew_type, self.crew(crew_type))

    def _get_available_crew_types(self):
        """private method that gets the available crew types.

        :return: returns a list of crew types
        :rtype: list
        """
        xpath_str = "//h4[@class='dataHeaderWithBorder']"
        a_types = [h4.get("id") for h4 in self._fullcredits_page.xpath(xpath_str)]
        return a_types

    def _get_meta_data(self):
        """private method that gets the movie metadata from the page.

        the metadata section contains the year, rating and runtime.

        :return: dict containing the metadata.
            ex. {'year': '2021', 'rating': 'PG-13', 'runtime': '2h 35min'}
        :rtype: dict
        """
        # the metadata section is in a ul element
        ul_elem = self._page.xpath(
            "(//ul[@data-testid='hero-title-block__metadata'])[1]"
        )[0]

        md_list = [remove_spaces(li.text_content()) for li in ul_elem.xpath("li")]

        # put the metadata list into a dictionary
        md_dict = dict()
        md_dict["year"] = md_list[0][: len(md_list[0]) // 2]
        md_dict["rating"] = md_list[1][: len(md_list[1]) // 2]
        md_dict["runtime"] = md_list[2]

        return md_dict

    def _get_castcrew_list(self, header_id):
        """private method that gets

        :param header_id: the id of the h4 element in the page
        :type header_id: str
        :return: a list of tuples, containing the person and their role.
            ex. [('Denis Villeneuve', '(directed by)')]
        :rtype: list
        """
        xpath_str = "//h4[@id='{0}'][1]".format(header_id)
        try:
            h4_elem = self._fullcredits_page.xpath(xpath_str)[0]
        except IndexError:
            return None

        if header_id == "cast":
            table_elem = h4_elem.xpath(
                "following-sibling::table[@class='cast_list'][1]"
            )[0]
            row_xpath = "tr"
        else:
            table_elem = h4_elem.xpath(
                "following-sibling::table[contains(@class, " "'simpleCreditsTable')][1]"
            )[0]
            row_xpath = "tbody//tr"
        person_list = list()
        person_role = tuple()

        for tr in table_elem.xpath(row_xpath):
            if len(tr.xpath("td")) == 2:
                person_role = remove_spaces(
                    tr.xpath("td")[0].text_content()
                ), remove_spaces(tr.xpath("td")[1].text_content())
                person_list.append(person_role)
            elif len(tr.xpath("td")) == 3:
                person_role = remove_spaces(
                    tr.xpath("td")[0].text_content()
                ), remove_spaces(tr.xpath("td")[2].text_content())
                person_list.append(person_role)
            elif len(tr.xpath("td")) == 4:
                # the cast table contains 4 tds
                person_role = remove_spaces(
                    tr.xpath("td")[1].text_content()
                ), remove_spaces(tr.xpath("td")[3].text_content())
                person_list.append(person_role)
        return person_list

    @property
    def description(self):
        """movie description

        :return: a short description of the movie
        :rtype: str
        """
        span_elem = self._page.xpath("(//span[@data-testid='plot-xl'])[1]")[0]
        return span_elem.text_content()

    @property
    def summary(self):
        """movie summary

        :return: a summary of the movie
        :rtype: str
        """
        div_elem = self._page.xpath(
            "(//div[@data-testid='storyline-plot-summary'])[1]"
        )[0]
        return div_elem.text_content()

    @property
    def title(self):
        """title of the movie

        :return: the title of the movie
        :rtype: str
        """
        t_elem = self._page.xpath("(//h1[@data-testid='hero-title-block__title'])[1]")[
            0
        ]
        return t_elem.text_content()

    @property
    def year(self):
        """year the movie was released

        :return: release year of the movie
        :rtype: str
        """
        return self._meta_data.get("year", None)

    @property
    def rating(self):
        """movie rating

        :return: movie rating
        :rtype: str
        """
        return self._meta_data.get("rating", None)

    @property
    def runtime(self):
        """runtime of the movie

        :return: movie runtime
        :rtype: str
        """
        return self._meta_data.get("runtime", None)

    @property
    def release_date(self):
        """release date

        :return: release date of the movie
        :rtype: str
        """
        a_elem = self._page.xpath(
            "//a[contains(@href, 'releaseinfo?ref_=tt_dt_rdat') and "
            "contains(@class, 'ipc-metadata-list-item__list-content-item')][1]"
        )[0]
        return a_elem.text_content()

    @property
    def directed_by(self):
        """directors

        :return: a list of tuples
            ex. [('Denis Villeneuve', '(directed by)')]
        :rtype: list
        """
        directors = self.crew("director")
        return directors

    @property
    def written_by(self):
        """writers

        :return: a list of tuples
            ex. [('Writer', '(Role)')]
        :rtype: list
        """
        writers = self.crew("writer")
        return writers

    @property
    def cast(self):
        """cast members

        :return: a list of tuples
            ex. [('Cast member', '(Role)')]
        :rtype: list
        """
        cast = self._get_castcrew_list("cast")
        return cast

    def crew(self, crew_type):
        """generic method to get a list of crew members

        this is a method to get a list of the members from any of the
        available crew types. the constructor uses this method to
        auto-generate some of the crew properties.

        ex.
        tite.editor
        title.music_department

        _get_available_crew_types method gets a list of the
        available crew types.
        ex.

        :param crew_type: this is one of the h4 ids for the
            crew categories on the page
        :type crew_type: str
        :return: returns a list of tuples
            ex. [('Crew member', '(Role)')]
        :rtype: list
        """
        crew_list = self._get_castcrew_list(crew_type)
        return crew_list

    def __repr__(self) -> str:
        return "<movies.title - {}>".format(self.title)

    def __str__(self) -> str:
        return self.title
