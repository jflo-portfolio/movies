import re

from movies.data.movie_data import MovieData
from movies.utils import remove_spaces


class Celeb(MovieData):
    """This is a class representing a celeb.

    :param celeb_name: celebrity name
    :type celeb: str
    """

    def __init__(self, celeb_name):
        """Constructor method"""
        super().__init__()
        self._url = super().search(celeb_name, qtype="celeb")
        self._page = self._parse_page(self._url)
        self._bio_url = self._url + "bio"
        self._bio_page = self._parse_page(self._bio_url)

    @property
    def name(self):
        """name of the celeb

        :return: the name of the celeb
        :rtype: str
        """
        h_elem = self._page.xpath('(//h1[@class="header"])[1]')[0]
        return re.sub(r"\s+", " ", h_elem.text_content().strip())

    @property
    def overview(self):
        r"""celebrity overview

        gets data from the overview table
        contain things like Height, Birth Name, Nickname
        :return: returns a dictionary containing celeb overview data
            ex.
            {'Birth Name': 'Mark Richard Hamill', 'Height': '5\' 7¾" (1.72 m)'}
        :rtype: dict
        """
        t_elem = self._bio_page.xpath("(//table[@id='overviewTable'])[1]")[0]

        overview_d = dict()
        for tr in t_elem.xpath("descendant::tr"):
            key = remove_spaces(tr.xpath("td[1]")[0].text_content())
            value = remove_spaces(tr.xpath("td[2]")[0].text_content())
            overview_d[key] = value
        return overview_d

    @property
    def bio(self):
        """celebrity bio

        :return: celebrity biography
        :rtype: str
        """
        h_elem = self._bio_page.xpath("(//h4[contains(., 'Bio')])[1]")[0]
        return remove_spaces(h_elem.xpath("following-sibling::div")[0].text_content())

    @property
    def filmography(self):
        """filmography

        :return: list of films/shows/etc the celeb has been in
        :rtype: list
        """
        f_rows = self._page.xpath("//div[contains(@class, 'filmo-row')]")
        f_list = list()
        for f in f_rows:
            f_list.append(remove_spaces(f.xpath("descendant::a")[0].text_content()))
        return f_list

    def __repr__(self) -> str:
        return "<movies.celeb - {}>".format(self.name)

    def __str__(self) -> str:
        return self.name
