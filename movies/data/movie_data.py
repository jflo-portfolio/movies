import urllib.parse
from io import StringIO

import requests
from lxml import html


class MovieData:
    """Base MovieData Class

    sets up a requests sessions
    contains convenience methods to parse a page and search imdb
    """

    def __init__(self):
        """Constructor method"""
        self._sess = requests.Session()

    def _parse_page(self, url):
        """Method to parse an html page

        :param url: a url
        :type url: str
        :return: returns an _ElementTree object so we can
            find/get contents of the page.
        :rtype: class `lxml.etree._ElementTree`
        """
        res = self._sess.get(url)
        fobj = StringIO(res.text)
        return html.parse(fobj)

    def search(self, query, qtype=None):
        """Search imdb

        :param query: query to search for celeb or title
        :type query: str
        :param qtype: celeb or title
        :type qtype: str or None, optional
        :return: returns the url of the first search result
        :rtype: str
        """
        search_str = urllib.parse.quote_plus(query)

        if qtype == "title":
            q_url = "https://www.imdb.com/find?s=tt&q={0}".format(search_str)

        elif qtype == "celeb":
            q_url = "https://www.imdb.com/find?s=nm&q={0}".format(search_str)

        search_page = self._parse_page(q_url)
        result = search_page.xpath("(//td[@class='result_text'])[1]/a")[0]

        return "https://www.imdb.com{0}".format(result.attrib["href"])
