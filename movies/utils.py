import re


def remove_spaces(orig_str):
    """Remove repetitive spaces in a string

    :param orig_str: input string
    :type orig_str: str
    :return: string with unnecessary spaced removed
    :rtype: str
    """
    new_str = re.sub(r"\s+", " ", orig_str)
    return new_str.strip()
